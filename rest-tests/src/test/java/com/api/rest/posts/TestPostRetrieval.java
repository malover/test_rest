package com.api.rest.posts;

import com.api.rest.RestTestBase;
import com.api.rest.model.ResourceUri;
import com.api.rest.model.posts.serializable.Post;
import com.api.rest.utils.RequestBuilderJsonWithBean;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import org.testng.annotations.Test;

import java.net.HttpURLConnection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by alex.liubozhenko on 13/01/2016.
 */
public class TestPostRetrieval extends RestTestBase {

    @Test(description = "example with response analysis via JsonPath")
    public void shouldHaveMoreThanOneUniquePostCreator() {
        JsonPath response =
                given().then()
                        .expect()
                        .contentType(ContentType.JSON)
                        .statusCode(HttpURLConnection.HTTP_OK)
                        .when()
                        .get(new ResourceUri().posts().toString())
                        .body().jsonPath();
        List<String> userIds = response.get("userId");
        Set<String> uniqueIds = new HashSet<>(userIds);
        assertThat("posts are created by  more than 1 unique authors", uniqueIds.size(), greaterThan(1));
    }

    @Test(description = "example with response assertion inline")
    public void shouldHaveMoreThanOneUniquePostCreator2() {
        given().then()
                .expect()
                .contentType(ContentType.JSON)
                .statusCode(HttpURLConnection.HTTP_OK)
                .body("userId.size", greaterThan(1))
                .when()
                .get(new ResourceUri().posts().toString())
                .body().jsonPath();
    }

    @Test(enabled = false, description = "example with automatic serialization. Test fails, because of application bug")
    public void shouldBeAbleToRetrieveJustCreatedPost() {
        Post newPost = new Post().builder().
                userId(UUID.randomUUID().toString()).
                title("post#" + String.valueOf(System.currentTimeMillis())).
                body("happy bytes!happy bytes!").build();
        RequestSpecification request = RequestBuilderJsonWithBean.build(newPost);

        Post createdPost = given().spec(request).port(RestAssured.UNDEFINED_PORT)
                .then()
                .expect()
                .contentType(ContentType.JSON)
                .statusCode(HttpURLConnection.HTTP_CREATED)
                .when()
                .post(new ResourceUri().posts()).as(Post.class);

        Post retrievedPost = given().then()
                .expect()
                .contentType(ContentType.JSON)
                .statusCode(HttpURLConnection.HTTP_OK)
                .when()
                .get(new ResourceUri().postById(createdPost.getId())).as(Post.class);

        assertEquals("entity was created incorrectly", createdPost, retrievedPost);
    }

    @Test(description = "example with deserialization of collection of items")
    public void shouldBeAbleToDeserializeCollectionOfItems() {
        Response jsonResponse =
                given().then()
                        .expect()
                        .contentType(ContentType.JSON)
                        .statusCode(HttpURLConnection.HTTP_OK)
                        .when()
                        .get(new ResourceUri().posts())
                        .then().contentType(ContentType.JSON).extract().response();

        String jsonAsString = jsonResponse.asString();

        List<Post> posts =  RequestBuilderJsonWithBean.parse(Post.class, jsonAsString);

        assertThat(posts.size(), is(equalTo(100)));
    }

}
