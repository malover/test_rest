package com.api.rest.posts;

import com.api.rest.RestTestBase;
import com.api.rest.model.ResourceUri;
import com.api.rest.model.posts.serializable.Post;
import com.api.rest.utils.RequestBuilderJsonWithBean;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.specification.RequestSpecification;
import org.testng.annotations.Test;

import java.net.HttpURLConnection;
import java.util.*;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by alex.liubozhenko on 11/01/2016.
 */
public class TestPostCreation extends RestTestBase {

    /**
     * 'example with intentional serialization' - that means, that JSON object is serialized first,
     * then set as input argument to spec() method.
     *
     * The advantage is, that in RequestBuilderJsonWithBean.class you can specify the policy for fields serialization
     * (e.g.- serialise only NOT NULL fields) and still use the same bean object for deserialization,
     * that's more flexible, than using @JsonIgnore annotation.
     *
     */
    @Test(description = "example with intentional serialization")
    public void shouldBeAbleToCreatePostWithIntentionalSerialization(){

        Post newPost = new Post().builder().
                userId(UUID.randomUUID().toString()).
                title("post#"+String.valueOf(System.currentTimeMillis())).
                body("happy bytes!happy bytes!").build();
        RequestSpecification request = RequestBuilderJsonWithBean.build(newPost);

        given().spec(request).port(RestAssured.UNDEFINED_PORT)
                .then()
                .expect()
                .contentType(ContentType.JSON)
                .statusCode(HttpURLConnection.HTTP_CREATED)
                .when()
                .post(new ResourceUri().posts().toString())
                .body().jsonPath();
    }

    @Test(description = "example with automatic serialization")
    public void shouldBeAbleToCreatePostWithAutoSerialization(){
        String etalonUserId = UUID.randomUUID().toString();
        String etalonTitle = "post#"+String.valueOf(System.currentTimeMillis());
        String etalonBody = "happy 8 bytes! happy 8 bytes!";

        Post newPost = new Post().builder().
                userId(etalonUserId).
                title(etalonTitle).
                body(etalonBody).build();

        given()
                .contentType(ContentType.JSON)
                .body(newPost)
                .then()
                .expect()
                .body(
                        "userId", equalTo(etalonUserId),
                        "title", equalTo(etalonTitle),
                        "body", equalTo(etalonBody),
                        "id", is(notNullValue()))
                .statusCode(HttpURLConnection.HTTP_CREATED)
                .when()
                .post(new ResourceUri().posts());
    }

    @Test(description = "example with automatic serialization")
    public void shouldBeAbleToCreatePostWithAutoSerializationDeserialization(){
        Post newPost = new Post().builder().
                userId(UUID.randomUUID().toString()).
                title("post#"+String.valueOf(System.currentTimeMillis())).
                body("happy 8 bytes! happy 8 bytes!").build();

        Post createdPost = given()
                .contentType(ContentType.JSON)
                .body(newPost)
                .then()
                .expect()
                .statusCode(HttpURLConnection.HTTP_CREATED)
                .when()
                .post(new ResourceUri().posts()).as(Post.class);

        assertEquals("entity was created incorrectly", newPost, createdPost);
    }

}
