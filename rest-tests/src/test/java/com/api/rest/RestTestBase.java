package com.api.rest;

import com.api.rest.config.PropertyNames;
import com.api.rest.config.TestConfiguration;
import com.jayway.restassured.RestAssured;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;
import java.sql.SQLException;

public class RestTestBase {

    private static final Logger logger = LoggerFactory.getLogger(RestTestBase.class);

    @AfterMethod(alwaysRun = true)
    public void afterMethod() throws ClassNotFoundException, SQLException {
        logger.info("Stopping  tests execution..resetting rest-assured..");
        RestAssured.reset();
    }

    @BeforeMethod(alwaysRun = true)
    public void setUpTestProperties() throws IOException {
        logger.info("Initializing  properties");
        TestConfiguration.setSystemProperties();

        if (!System.getProperties().containsKey(PropertyNames.HOST_URL)) {
            Assert.fail("HOST_URL is not specified");
        }
    }

}