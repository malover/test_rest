package com.api.rest.model;

public enum AuthErrors {
    INVALID_REQUEST("invalid_request"), INVALID_SCOPE("invalid_scope"), INVALID_GRANT("invalid_grant"), INVALID_CLIENT("invalid_client"),
    UNAUTHORIZED_CLIENT("unauthorized_client");

    private String name;

    AuthErrors(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
