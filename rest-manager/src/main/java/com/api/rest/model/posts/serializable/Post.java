package com.api.rest.model.posts.serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.experimental.Builder;

/**
 * Created by alex.liubozhenko on 12/01/2016.
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Post {

    @NonNull
    private String userId;
    @NonNull
    private String title;
    @NonNull
    private String body;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Post)) return false;

        Post post = (Post) o;

        if (userId != null ? !userId.equals(post.userId) : post.userId != null) return false;
        if (title != null ? !title.equals(post.title) : post.title != null) return false;
        return !(body != null ? !body.equals(post.body) : post.body != null);

    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (body != null ? body.hashCode() : 0);
        return result;
    }
}
