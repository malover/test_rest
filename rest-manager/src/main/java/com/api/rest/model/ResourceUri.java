package com.api.rest.model;

import com.jayway.restassured.RestAssured;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;


/**
* Provides URIs for various resources.
*/
public class ResourceUri {

    /**
     *
     * @return /posts
     */
    public URI posts() {
        return postsPath().build();
    }

    /**
     *
     * @param postId
     * @return /posts/{postId}
     */
    public URI postById(String postId) {
        return postByIdPath().build(postId);
    }

    /**
     *
     * @param postId
     * @return /posts/{postId}/comments
     */
    public URI postComments(String postId) {
        return postByIdPath().path("comments").build(postId);
    }

    /**
     *
     * @param postId
     * @return 	/comments?postId=1
     */
    public URI commentsByPostId(String postId) {
        return comments().queryParam("postId", postId).build();
    }

    /**
     *
     * @param userId
     * @return /posts?userId=1
     */
    public URI postsByUserId(String userId) {
        return postsPath().queryParam("userId",userId).build();
    }


    private static UriBuilder baseBuilder() {
        return UriBuilder.fromUri(RestAssured.baseURI).path(RestAssured.basePath);
    }

    private static UriBuilder postsPath() {
        return baseBuilder().path("posts");
    }

    private static UriBuilder comments() {
        return baseBuilder().path("comments");
    }

    private static UriBuilder postByIdPath() {
        return baseBuilder().path("posts").path("{postId}");
    }

}
