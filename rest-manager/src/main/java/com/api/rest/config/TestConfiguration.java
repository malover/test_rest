package com.api.rest.config;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.filter.log.LogDetail;
import com.jayway.restassured.filter.log.RequestLoggingFilter;
import com.jayway.restassured.filter.log.ResponseLoggingFilter;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public final class TestConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(TestConfiguration.class);
    private static String CONFIG_FILE_NAME = "global.properties";

    private static PropertiesConfiguration read() {

        PropertiesConfiguration configuration;
        try {
            configuration = new PropertiesConfiguration();
            configuration.setEncoding("UTF-8");
            configuration.load(CONFIG_FILE_NAME);
        } catch (org.apache.commons.configuration.ConfigurationException e) {
            throw new RuntimeException("Cannot obtain config", e);
        }

        return configuration;
    }

    public static void setSystemProperties() {
        final PropertiesConfiguration configuration = read();
        setUpRestAssured(configuration);
        String rest_url = getOrThrow(configuration.getString(PropertyNames.HOST_URL), "restapi url isn't specified");
//      String rest_port = configuration.getString(PropertyNames.PORT);
        System.setProperty(PropertyNames.HOST_URL, rest_url);
        System.setProperty(PropertyNames.PORT, String.valueOf(RestAssured.UNDEFINED_PORT));
    }

    private static void setUpRestAssured(PropertiesConfiguration configuration) {
        logger.info("setting rest-assured  up ...");
        RestAssured.baseURI=configuration.getString(PropertyNames.HOST_URL);
        RestAssured.filters(new RequestLoggingFilter(LogDetail.ALL), new ResponseLoggingFilter(LogDetail.ALL));
    }

    private static <T> T getOrThrow(T value, String exceptionMessage) {
        if(value == null) {
            throw new ConfigurationException(exceptionMessage);
        }
        return value;
    }

}
