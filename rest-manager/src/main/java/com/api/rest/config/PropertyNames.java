package com.api.rest.config;

/**
 * User: alex.lyubozhenko
 */
public class PropertyNames {
    public static final String HOST_URL = "restapiUrl";
    public static final String PORT = "restPort";

}
