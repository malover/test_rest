package com.api.rest.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.specification.RequestSpecification;
import org.testng.Assert;

import java.io.IOException;
import java.util.List;

import static com.jayway.restassured.config.DecoderConfig.decoderConfig;
import static com.jayway.restassured.config.EncoderConfig.encoderConfig;
import static com.jayway.restassured.config.RestAssuredConfig.newConfig;

public final class RequestBuilderJsonWithBean {

	public static RequestSpecification build(Object beanObject) {

		return build(beanObject, JsonInclude.Include.ALWAYS);
	}

    public static <T> RequestSpecification build(T beanObject, JsonInclude.Include inclusionType) {
        String  jsonString = null;
        try {
            jsonString = serialize(beanObject, inclusionType);
        } catch (JsonMappingException e) {
            e.printStackTrace();
        }
        RestAssuredConfig httpConfig = newConfig()
                .encoderConfig(encoderConfig().defaultContentCharset("UTF-8"))
                .decoderConfig(decoderConfig().defaultContentCharset("UTF-8"));

        return
                new RequestSpecBuilder()
                        .setConfig(httpConfig)
                        .setContentType(ContentType.JSON)
                        .setBody(jsonString)
                        .build();
    }

    private static String serialize(Object beanObject, JsonInclude.Include inclusionType) throws JsonMappingException {
        String stringBody = null;
        try {
            stringBody = new ObjectMapper()
                    .setSerializationInclusion(inclusionType)
                    .writeValueAsString(beanObject);
        } catch (JsonMappingException e) {
            Assert.fail("Unparsable Json");
        } catch (IOException e) {
            Assert.fail("Unexpected IO exception");
        }
        return stringBody;
    }

    public static <T> List<T> parse(Class<T> clazz, String jsonString, JsonInclude.Include inclusionType) {
        ObjectMapper objectMapper = new ObjectMapper();

        List<T> receivedBeans = null;
        try {
            receivedBeans = objectMapper
            .setSerializationInclusion(inclusionType)
            .readValue(jsonString, objectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return receivedBeans;
    }

    public static <T> List<T> parse(Class<T> clazz, String jsonString) {
        return parse(clazz,jsonString,JsonInclude.Include.NON_NULL);
    }

	private RequestBuilderJsonWithBean() {}
}
